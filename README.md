# docker-ubuntu

NOTE: In Settings > CI/CD > Pipeline triggers, trigger "Build when dotfiles has been updated" revoked due to quota limitations

![](https://gitlab.com/psicho/docker-ubuntu/badges/master/pipeline.svg?key_text=Ubuntu%20build&key_width=90)

Dockerfile for Ubuntu image with basic applications and dotfiles

## Build & push image to GitLab Registry
Will be done automatically via GitLab CI.

## Prerequisites
Needs font `CaskaydiaCove NF`, see
 * https://gitlab.com/psicho/dotfiles#install-steps
 * https://github.com/psicho2000/wiki/blob/master/Settings/Terminal/Glyphs.txt

## Run
```
docker run --rm -it registry.gitlab.com/psicho/docker-ubuntu
```

## Known issues in the context of docker-ubuntu
 * During image build, when unminimize is executed we get:  
   `dpkg-query: error: --search needs at least one file name pattern argument`
   This has no negative impact, though.
 * When starting a tmux session for the first time, we get 
   `error connecting to /tmp/tmux-1000/default (No such file or directory)`
   That does not happen if starting the next time. Seems to be a general tmux issue, reproducible on non docker setups.
 * When starting a tmux session directly from shell (via `tn` or `tmux`), Nerd Fonts are not rendered.
   Workaround: use the python launcher (`tmux-select-session.py`, called via `t`) to create the first, third, fourth, ... session or to connect to an existing session.
 * When starting vim, ycm complains. Reason: ycm not installed due to image size issues, but can be done with `~/dotfiles/install/install-youcompleteme.sh`
