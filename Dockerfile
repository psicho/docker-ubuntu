FROM ubuntu:22.04

USER root

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get upgrade --yes

# prevent 'debconf: delaying package configuration, since apt-utils is not installed'
# one warning will occur though when apt-utils is being installed
RUN apt-get install --yes apt-utils

RUN apt-get install --yes \
        cron \
        curl \
        # nslookup, dig, host
        dnsutils \
        htop \
        # traceroute
        inetutils-traceroute \
        # lsmod is not available in containers - need this workaround
        kmod \
        lsb-release \
        lsof \
        man \
        nano \
        # visual disk usage
        ncdu \
        # ifconfig
        net-tools \
        # pstree
        psmisc \
        sudo \
        telnet \
        # tzselect, for exa to have access to timezones
        tzdata \
        tcpflow \
        wget \
    && \
    # the ubuntu docker image is minimized by default
    # known issue: https://bugs.launchpad.net/cloud-images/+bug/1996489
    yes | unminimize && \
    apt-get clean && \
    rm --recursive --force /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN useradd --home-dir /home/ubuntu --shell /bin/bash ubuntu && \
    install --owner ubuntu --group ubuntu --directory /home/ubuntu && \
    echo "ubuntu ALL=NOPASSWD: ALL" >> /etc/sudoers

USER ubuntu

# install dotfiles
RUN cd "$HOME" && \
    curl --location --remote-name https://vipc.de/bootstrap && \
    TERM=xterm-256color bash bootstrap --silent --name docker-ubuntu --all --java-version 17 && \
    echo 'export USER="$(whoami)"' >> /home/ubuntu/.config/dotfiles/custom.sh && \
    echo 'export SHELL="/bin/zsh"' >> /home/ubuntu/.config/dotfiles/custom.sh

CMD ["zsh", "--login"]
